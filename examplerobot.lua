
require "gameData"

require "robot"

ExampleRobot = Robot:new()

local cond = ExampleRobot.conditions

function cond.move(data) 
  return true
end

function cond.rotate(data)
  return true, "random"
end



ExampleRobot.actionList = {
    "move",
  "rotate",
    "move",
  "rotate",
    "move",
  "rotate",
    "move",
  "rotate",
}



return ExampleRobot