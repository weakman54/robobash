
Robot = {}

function Robot:new(obj)
  obj = obj or {}
  
  setmetatable(obj, Robot)
  self.__index = self
  
  obj.color = obj.color or {255, 000, 000}
  obj.facing = obj.facing or {x=1, y=0}
  obj.conditions = {}
  
  return obj
end


function Robot:draw(x, y)
  love.graphics.setColor(self.color)
  love.graphics.rectangle("fill", (x-1)*Tile_Size, (y-1)*Tile_Size, Tile_Size, Tile_Size)
  
  love.graphics.setColor(0, 0, 0)
  love.graphics.circle("fill", (x-1)*Tile_Size + Tile_Size/2 + self.facing.x * Tile_Size/4, (y-1)*Tile_Size + Tile_Size/2  + self.facing.y * Tile_Size/4, Tile_Size/8)
end

