
require "gameData"

Tile = {}

Tile.__index = Tile

function Tile:draw(x, y)
  love.graphics.setColor(255, 255, 255)
  love.graphics.rectangle("line", (x-1)*32, (y-1)*32, 32, 32)
  
  if self.robot then
    self.robot:draw(x, y)
  end 
end

Grid = {}

for x=1, Grid_Width do
  table.insert(Grid, {})
  
  for y=1, Grid_Height do
    table.insert(Grid[x], setmetatable({}, Tile))
  end
end

function Grid:draw()
  for x, col in ipairs(self) do
    for y, tile in ipairs(col) do
      tile:draw(x, y)
    end
  end
end