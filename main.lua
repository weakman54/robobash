--Edited
require "gameData"
require "Grid"


robot1 = require "ExampleRobot"

function love.load(arg)
  if arg[#arg] == "-debug" then require("mobdebug").start() end
  local x = 1--math.floor(Grid_Width/2)
  local y = 1--math.floor(Grid_Height/2)
  Grid[x][y].robot = robot1
  robot1.x = x
  robot1.y = y
end


actions = {}

function actions.move(robot)
  print("Move")
  local x = robot.x 
  local y = robot.y
    
  local nx = x + robot.facing.x
  local ny = y + robot.facing.y
  
  if Grid[nx] and Grid[nx][ny] then
    Grid[x][y].robot = nil
    Grid[nx][ny].robot = robot
    
    robot.x = nx
    robot.y = ny
    
    return true
  end
  
  return false
end

function actions.rotate(robot)
  _, data = robot.conditions.rotate()
  
  print("rotate: ", _, data)
end



function turn()
  print("Turn")
  local actionPoints = Action_Points_Default
  for _, action in ipairs(robot1.actionList) do
    
  
  end
end


local counter = 0
local index = 1
local actionPoints = 4

function love.update(dt)
  counter = counter + dt
  
  if counter >= 1 then
    counter = counter - 1
    index = index + 1
    if index > #robot1.actionList then
      index = 1
      actionPoints = 4
    end
    
    action = robot1.actionList[index]
    
    if actionPoints >= 1 and robot1.conditions[action] then
      if actions[action](robot1) then
        actionPoints = actionPoints - 1
      end
    end
    
  end
end


function love.draw()
  Grid:draw()
end